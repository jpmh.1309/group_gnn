import torch

from datasets.gnn.utils import process_features, process_groups


def read_dataset():
    groups_file = "data/cocktail_party/groups.txt"
    features_file = "data/cocktail_party/features.txt"
    all_groups, all_features = {}, {}

    with open(groups_file, "r") as f:
        for line in f:
            if len(line) == 0:
                continue
            stamp = line.split(" ")[0]
            all_groups[stamp] = process_groups(line)

    with open(features_file, "r") as f:
        for line in f:
            if len(line) == 0:
                continue
            stamp = line.split(" ")[0]
            if stamp in all_groups and stamp not in all_features:
                all_features[stamp] = process_features(
                    line, all_groups[stamp], expected_num_features=5
                )

    if len(all_groups) != len(all_features):
        raise ValueError("Mismatched lengths")

    stamps = all_groups.keys()
    data = []
    for stamp in stamps:
        groups = all_groups[stamp]
        features = list(all_features[stamp].items())
        people = [f[0] for f in features]
        pos = torch.zeros(size=(len(people), 3))
        head_norm = torch.zeros(size=(len(people), 3))
        body_norm = torch.zeros(size=(len(people), 3))
        x = torch.zeros(size=(len(people), len(features[0][1]) - 4))
        y = torch.zeros(size=(len(people), 1))
        for i, person in enumerate(people):
            pos[i] = torch.tensor(features[i][1][:2] + [0])
            head_norm[i][0] = torch.cos(torch.tensor(features[i][1][2]))
            head_norm[i][1] = torch.sin(torch.tensor(features[i][1][2]))
            body_norm[i][0] = torch.cos(torch.tensor(features[i][1][3]))
            body_norm[i][1] = torch.sin(torch.tensor(features[i][1][3]))
            x[i] = torch.tensor(features[i][1][4:])
            for j, group in enumerate(groups):
                if person in group:
                    y[i] = torch.tensor(j)

        data.append([stamp, pos, head_norm, body_norm, x, y])

    # return combined lists
    return data
