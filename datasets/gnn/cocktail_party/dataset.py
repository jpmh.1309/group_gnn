import torch
from torch_geometric.data import InMemoryDataset, Data

from datasets.gnn.cocktail_party.read_dataset import read_dataset


class CocktailPartyDataset(InMemoryDataset):
    def __init__(self, root, transform=None, pre_transform=None, device="cpu"):
        self.device = device
        super(CocktailPartyDataset, self).__init__(root, transform, pre_transform)
        self.data, self.slices = torch.load(self.processed_paths[0])
        self.data.to(device)

    @property
    def raw_file_names(self):
        return ["data/cocktail_party/raw"]

    @property
    def processed_file_names(self):
        return ["data.pt"]

    @property
    def num_classes(self):
        return 6

    def process(self):
        # Read data into huge `Data` list.
        data_list = []
        raw_data = read_dataset()
        for (frame_id, pos, head_norm, body_norm, x, y) in raw_data:
            data = Data(
                x=x,
                y=y,
                pos=pos,
                head_norm=head_norm,
                body_norm=body_norm,
                frame_id=frame_id,
            )
            data_list.append(data)

        if self.pre_filter is not None:
            data_list = [data for data in data_list if self.pre_filter(data)]

        if self.pre_transform is not None:
            data_list = [self.pre_transform(data) for data in data_list]

        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])
