from collections import defaultdict

import torch


def collate_fn(data):
    dyad_dict = defaultdict(list)
    context_dict = defaultdict(list)
    num_people_dict = defaultdict(list)
    target_dict = defaultdict(list)
    target_list = []

    for sample in data:
        (dyad, context, people), target = sample
        num_people = int(people.item())
        dyad_dict[num_people].append(dyad)
        context_dict[num_people].append(context)
        num_people_dict[num_people].append(people)
        target_dict[num_people].append(target)

    dyad_dict = {k: torch.stack(v) for k, v in dyad_dict.items()}
    context_dict = {k: torch.stack(v) for k, v in context_dict.items()}
    num_people_dict = {k: torch.stack(v) for k, v in num_people_dict.items()}

    # ensures reproducible ordering
    keys = sorted(target_dict.keys())
    for k in keys:
        target_list.extend(target_dict[k])

    x = [(dyad_dict[k], context_dict[k], num_people_dict[k]) for k in keys]
    y = torch.stack(target_list)

    return x, y
