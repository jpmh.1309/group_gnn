import math

import torch


def move_angle_to_PI_range(a):
    """
    Helper function to move an angle to the [-pi, pi) range
    :param a: input angle (in radians)
    :return: transformed angle
    """
    result = (a + 2 * math.pi) % (2 * math.pi)
    if result > math.pi:
        result -= 2 * math.pi
    return result


def min_angle_diff(a1, a2):
    """
    Helper function to compute the min. angle difference: a1 - a2
    :param a1: input angle (in radians)
    :param a2: input angle (in radians)
    :return: difference
    """
    a1 = move_angle_to_PI_range(a1)
    a2 = move_angle_to_PI_range(a2)
    d = a1 - a2
    if d > math.pi:
        d = d - 2 * math.pi
    elif d < -math.pi:
        d = d + 2 * math.pi
    return d


def recover_angle(x, y):
    """
    Helper function to return angle from cos and sin components
    :param x: cos(theta)
    :param y: sin(theta)
    :return: theta
    """

    mag = torch.sqrt(x ** 2 + y ** 2)
    x /= mag + 1e-15  # prevents div by 0
    y /= mag + 1e-15  # prevents div by 0

    if x == 0:
        return torch.asin(y)
    elif y >= 0:
        return torch.acos(x)
    elif y < 0:
        return -torch.acos(x)
