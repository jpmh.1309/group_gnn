import torch


class RectangularAngle(object):
    """
    Convert the angle (theta) feature in an input tensor to its rectangular (2D) representation: cos(theta), sin(theta)
    """

    def __init__(self, angle_index):
        """
        Constructor
        :param angle_index: (int or list) position of the orientation feature in the second dim of the input tensors
        """
        if not isinstance(angle_index, list):
            self.angle_index = [angle_index]
        else:
            self.angle_index = angle_index

    def __call__(self, sample):
        """
        Transform a given sample (dyad_input, context_input)
        :param sample: AffinityDataset sample
        """
        if len(self.angle_index) == 0:
            return sample
        dyad_input, context_input = sample

        # compute new number of features, which dimensions we are keeping, and allocate output tensors
        final_dims = dyad_input.shape[-1] + len(self.angle_index)

        new_dyad = torch.zeros(
            (dyad_input.shape[0], final_dims), dtype=dyad_input.dtype
        )
        new_context = torch.zeros(
            (context_input.shape[0], final_dims), dtype=context_input.dtype
        )

        keep_dims = [
            x for x in range(dyad_input.shape[-1]) if x not in self.angle_index
        ]

        # copy same values
        for i, index in enumerate(keep_dims):
            new_dyad[:, i] = dyad_input[:, index]
            new_context[:, i] = context_input[:, index]

        # transform angles
        k = len(keep_dims)
        for i, index in enumerate(self.angle_index):
            new_dyad[:, k + i * 2] = torch.cos(dyad_input[:, index])
            new_dyad[:, k + i * 2 + 1] = torch.sin(dyad_input[:, index])
            new_context[:, k + i * 2] = torch.cos(context_input[:, index])
            new_context[:, k + i * 2 + 1] = torch.sin(context_input[:, index])

        return (new_dyad, new_context)

    def __repr__(self):
        return self.__class__.__name__ + "(angle_index={})".format(self.angle_index)
