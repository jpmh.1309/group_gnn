from joblib import Parallel, delayed
import multiprocessing

import torch
from torch.utils.data import Dataset

from datasets.dante.cocktail_party.read_dataset import read_dataset
from transforms import groups_to_affinity


def transform_group_sample(dyad_input, context_input, transform):
    """
    Helper function to transform a group sample
    :param dyad_input: dyad input (B x 2 x F tensor)
    :param context_input: context input (B x (P - 1) x F tensor)
    :param transform: transforms to apply to the sample or None
    :return: transformed sample as tuple
    """

    def transform_sample(i, dyad, context):
        d = dyad[i, :, :]
        c = context[i, :, :]

        new_sample = transform((d, c))
        return new_sample[0], new_sample[1]

    if transform is None:
        return dyad_input, context_input

    new_dyad, new_context = [], []

    # num_cores = 8
    # results = Parallel(n_jobs=num_cores, verbose=0, prefer="threads")(
    #     delayed(transform_sample)(i, dyad_input, context_input) for i in list(range(dyad_input.shape[0])))
    for i in range(dyad_input.shape[0]):
        new_sample = transform_sample(i, dyad_input, context_input)
        new_dyad.append(new_sample[0])
        new_context.append(new_sample[1])

    # new_dyad, new_context = zip(*results)
    new_dyad = torch.stack(new_dyad, 0)
    new_context = torch.stack(new_context, 0)
    return new_dyad, new_context


class CocktailPartyDataset(Dataset):
    """
    Dataset for classifying two people as part of the same group or not.
    The target values correspond to affinities in the interaction graph.
    :note: This class follows the guidelines in https://pytorch.org/tutorials/beginner/data_loading_tutorial.html
    """

    def __init__(
        self,
        angles,
        transform=None,
        dtype=torch.float32,
        device=None,
        name="cocktail_party",
    ):
        """
        Constructor
        :param angle: determines which angle features to include (None, head, body, head + body)
        :param transform: transform to apply to samples (pass None for no transform)
        :param dtype: sample tensor type (by default, we output floats in single precision)
        :param device: determines whether to use CPU or GPU (default is CPU)
        """

        self.angles = angles

        self.transform = transform
        self.dtype = dtype
        self.device = device

        self.stamps = []

        self.all_groups, self.all_features, self.all_num_people = read_dataset(
            angles, name=name
        )
        self.max_people = max(self.all_num_people.values())

        self.stamps = list(self.all_groups.keys())

        def make_example(k):
            stamp = self.stamps[k]
            groups = self.all_groups[stamp]
            features = self.all_features[stamp]

            N = self.all_num_people[stamp]
            F = len(list(features.values())[0])

            A = groups_to_affinity(groups, N, "cpu")

            for ID in features:
                features[ID] = torch.tensor(features[ID])

            num_pairs = N * (N - 1)
            max_pairs = self.max_people * (self.max_people - 1)
            dyad = torch.zeros((num_pairs, 2, F), device="cpu")
            context = torch.zeros((num_pairs, N - 2, F), device="cpu")
            target = -1 * torch.ones((max_pairs), device="cpu")  # pad target

            index = 0
            max_index = 0

            for i in range(N):
                for j in range(i + 1, N):
                    try:
                        dyad[index] = torch.stack([features[i], features[j]])
                        dyad[num_pairs - index - 1] = torch.stack(
                            [features[j], features[i]]
                        )
                        if N - 2 > 0:
                            context[index, : N - 2] = torch.stack(
                                [features[k] for k in range(N) if k not in [i, j]]
                            )
                            context[num_pairs - index - 1, : N - 2] = context[
                                index, : N - 2
                            ]
                        target[max_index] = A[i][j]
                        target[max_pairs - max_index - 1] = A[j][i]
                        index, max_index = index + 1, max_index + 1
                    except Exception as e:
                        print("e:", e)
                        print("N:", N)
                        print("i:", i)
                        print("j:", j)
                        print("max_index:", max_index)
                        print("features:", len(features))
                        print("features:", features)
                        raise e
                max_index += self.max_people - N

            dyad, context = transform_group_sample(dyad, context, self.transform)

            # from here on out, use the correct dtype, device for repeated query
            dyad = dyad.type(self.dtype)
            context = context.type(self.dtype)
            num_people = torch.tensor(N).type(self.dtype)
            target = target.type(self.dtype)
            return (dyad, context, num_people, target)

        num_cores = multiprocessing.cpu_count()
        print(f"Using {num_cores} cores to load data")
        results = Parallel(n_jobs=num_cores, verbose=1, prefer="threads")(
            delayed(make_example)(k) for k in list(range(len(self.stamps)))
        )
        self.examples = results

    def __getitem__(self, i):
        """
        Get ith example from the dataset
        :param i: requested index
        :return: example as a dictionary
        """
        dyad, context, num_people, target = self.examples[i]

        dyad = dyad.to(self.device)
        context = context.to(self.device)
        num_people = num_people.to(self.device)
        target = target.to(self.device)

        return (dyad, context, num_people), target

    def __len__(self):
        """
        Size of the dataset
        :return: number of samples
        """
        return len(self.examples)

    def get_max_people(self):
        return self.max_people

    def sample_weights(self, indices):
        # total elements in tensor is product of dimensions
        def get_total(target):
            return torch.flatten(target).shape[0]

        # total positive in tensor is count of 1s
        def get_pos(target):
            return int(target.sum().item())

        # total negative in tensor is total - positive
        def get_neg(target):
            return get_total(target) - get_pos(target)

        def getWeight(target, neg_weight, pos_weight):
            return (
                neg_weight * get_neg(target) + pos_weight * get_pos(target)
            ) / get_total(target)

        targets = [ex[-1] for i, ex in enumerate(self.examples) if i in indices]
        num_negative = sum([get_neg(t) for t in targets])
        num_positive = sum([get_pos(t) for t in targets])

        neg_weight, pos_weight = 1 / num_negative, 1 / num_positive
        samples_weight = [getWeight(t, neg_weight, pos_weight) for t in targets]
        samples_weight = torch.tensor(samples_weight, dtype=self.dtype)

        return torch.tensor(samples_weight)
