from torch.utils.data import Dataset


class Subset(Dataset):
    """
    Subset of a dataset at specified indices and with optional transform
    """

    def __init__(self, dataset, indices):
        """
        Constructor
        :param dataset: parent dataset
        :param indices: indices that we want to keep in the subset of the parent dataset
        """
        self.dataset = dataset
        self.indices = indices

    def __getitem__(self, i):
        """
        Get ith example from the dataset
        :param i: requested index
        :return: example as a dictionary
        """
        return self.dataset[self.indices[i]]

    def __len__(self):
        """
        Size of the subset
        :return: number of samples
        """
        return len(self.indices)
