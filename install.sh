#!/bin/bash

python_version=$(python3 --version)
echo "output: $python_version"

if [[ "$python_version" == *"3.8"* ]]
then
  echo "Using default Python3.8"
  echo "Creating a virtual environment called .venv3.8"
  python3 -m venv .venv3.8
else
  echo "Using Python 3.8"
  echo "Creating a virtual environment called .venv3.8"
  python3.8 -m venv .venv3.8
fi

echo "Activating virtual environment"
source .venv3.8/bin/activate

pip install --upgrade pip

echo "Installing base requirements"
pip install torch==1.8.1
pip install wheel
pip install pytorch-ignite
pip install tensorboard
pip install tensorboardX

echo "Installing pytorch geometric"

cuda_version=$(python -c "import torch; print(torch.version.cuda)")
echo "cuda_version: $cuda_version"

if [[ "$cuda_version" == *"None"* ]]
then
  cuda_version='cpu'
elif [[ cuda_version=='10.1 ]]
then
  cuda_version='cu101'
elif [[ cuda_version=='10.2 ]]
then 
  cuda_version='cu102'
elif [[ cuda_version=='11.0 ]]
then 
  cuda_version='cu110'
elif [[ cuda_version=='11.1 ]]
then 
  cuda_version='cu111'
fi

echo "Installing pytorch geometric with cuda version $cuda_version"
pip install torch-scatter -f https://pytorch-geometric.com/whl/torch-1.8.0+$cuda_version.html
pip install torch-sparse -f https://pytorch-geometric.com/whl/torch-1.8.0+$cuda_version.html
pip install torch-cluster -f https://pytorch-geometric.com/whl/torch-1.8.0+$cuda_version.html
pip install torch-spline-conv -f https://pytorch-geometric.com/whl/torch-1.8.0+$cuda_version.html
pip install torch-geometric
