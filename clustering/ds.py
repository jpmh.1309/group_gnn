import numpy as np
import warnings

import torch
import torch.nn as nn

from clustering.early_stopping import weight


def find_dominant_set(
    A, device, thresh=1e-5, max_iter=10000, min_eps=1e-15, verbose=False, add_noise=True
):
    """
    Find the most dominant set in the current graph (eq (13))
    Uses evolionary game theory to solve f(x) = x^TAx with x the weighted characteristic vector
    :param A: affinity matrix (NxN) with values in [0,1]
    :param allowed: indicator vector for the nodes that are valid to group together
    :param thresh: threshold for determining nodes in dominant set.
    :param max_iter: maximum number of iterations to find a solution
    :param min_eps: min. diff. between two consecutive estimates of the characteristic vector after which to stop the optimization
    :param verbose: print info. for debugging purposes?
    :param add_noise: add noise to the initial solution?
    :return: bool vector with the members of the dominant set and characteristic vector
    """
    if max_iter < 1:
        raise RuntimeError("max_iter must be greater than zero.")

    def f(x, A):
        """
        Dominant Sets quadratic programme function (eq. 11 in the paper)
        :param x: characteristic vector (N,1)
        :param A: affinity matrix (NxN)
        """
        v = torch.matmul(x.T, torch.matmul(A, x))
        return v[0, 0].item()

    N = A.shape[0]

    # we initialize x to the centroid of the standard simplex
    x = torch.full((N, 1), 1.0 / N, dtype=torch.double, device=device)
    # but add a bit of noise because without it is possible for the algorithm to fail in unweighted graphs with more
    # than one clique with the same cardinality
    # For example, if
    # A =
    # [[0. 0. 0. 1. 0. 1.]
    #  [0. 0. 1. 0. 1. 0.]
    #  [0. 1. 0. 0. 1. 0.]
    #  [1. 0. 0. 0. 0. 1.]
    #  [0. 1. 1. 0. 0. 0.]
    #  [1. 0. 0. 1. 0. 0.]]
    # then starting from the baricenter converges to the baricenter, which means that all nodes end up
    # being grouped together. The baricenter in that case is a saddle point, not a true clique.
    if add_noise:
        x = x + torch.randn(x.size(), dtype=torch.double, device=device) / 100
    x = x / torch.sum(x)

    if (
        torch.sum(torch.sum(A)).item() > 1e-20
    ):  # we only compute groups if the matrix is not all zeros

        eps = np.inf  # difference between old value and new value after updating x
        iters = 0

        # improve x to maximize f(x,A)
        while eps > min_eps and iters < max_iter:

            x_old = x.detach().clone()
            x = x * torch.matmul(A, x)
            x = x / torch.sum(
                torch.sum(x)
            )  # this is effectively the same as dividing by current value f(x,A)
            eps = torch.norm(x - x_old)

            if verbose:
                print(">> iters {}".format(iter))
                print("x_old:\n{}".format(x_old.T))
                print("x:\n{}".format(x.T))
                print("eps:\n{}".format(eps))

            iters += 1

        value = f(x, A)
        # print("value: {}".format(value))

        if iters >= max_iter and verbose:
            warnings.warn(
                "find_dominant_set() did not converge to a solution in {}/{} iterations (value = {}).\nx = {}\neps={}".format(
                    iter, max_iter, value, x.flatten(), eps
                )
            )

        if value > min_eps:
            members = (x > thresh).flatten()
            return members, x

    return torch.zeros_like(x, dtype=torch.bool, device=device).flatten(), x


def dominant_sets_clustering(
    A, n, device, thresh=1e-5, max_iter=10000, min_eps=1e-15, stop_gc=False, verbose=0
):
    """
    Iteratively compute dominant sets
    :param A: affinity matrix (NxN)
    :param n: number of real people in A
    :param thresh: threshold for determining nodes with find_dominant_set()
    :param max_iter: maximum number of iterations to find a solution with find_dominant_set()
    :param min_eps: min. diff. to stop the optimization in find_dominant_set()
    :param stop_gc: stop searching for clusters based on the global context
    :param verbose: print info. for debugging purposes? (0: no, 1: yes at the top level, 2: yes in all levels)
    :return: groups as a list of list of ids
    """
    with torch.no_grad():
        N = A.shape[0]
        affinity = A[:n, :n]
        groups = torch.zeros((N, N), dtype=torch.double, device=device)

        current_indices = torch.arange(n)

        members = torch.tensor([])
        num_members = 0

        iters = 0

        while affinity.shape[0] > 0:

            # remove those people that have been grouped already
            if num_members > 0:
                keep = torch.logical_not(members)
                keep_indices = torch.nonzero(keep.type(torch.int)).flatten()
                current_indices = current_indices[keep_indices]
                affinity = affinity[keep, :][:, keep]

                if affinity.shape[0] <= 1:
                    break

            if verbose:
                print("--- cluster {} ---\naffinity:\n {}".format(iters, affinity))

            # save last members
            last_members = members.detach().clone()

            # find dominant set
            members, x = find_dominant_set(
                affinity,
                device=device,
                thresh=thresh,
                max_iter=max_iter,
                min_eps=min_eps,
                verbose=(True if verbose > 1 else False),
            )
            num_members = torch.sum(members)

            if verbose:
                print("\nx: {}".format(x.T))
                print("thresh: {}".format(thresh))
                print("members: {}".format(members))
                print("num_members: {}".format(num_members))

            if num_members > 0:

                # compute group members
                if verbose:
                    print("members.nonzero().flatten():", members.nonzero().flatten())
                indices = members.nonzero().flatten()
                members_indices = current_indices[indices]

                if verbose:
                    print("members_indices: {}".format(members_indices))

                # check if we should stop extraction of groups based on the stopping criterion from Hung & Krose
                consistent_with_gc = True
                if stop_gc and last_members.shape[0] > 0:

                    m = torch.zeros(A.shape[0], dtype=torch.bool)
                    m[members_indices] = True
                    other_indices_list = torch.nonzero(
                        torch.logical_not(m).type(torch.int)
                    )

                    if verbose:
                        print(
                            "Checking group w.r.t. global context. Other indices: {}".format(
                                other_indices_list.tolist()
                            )
                        )
                    with torch.no_grad():
                        for index in other_indices_list.tolist():
                            tmp = m.detach().clone()
                            tmp[index] = True
                            w = weight(tmp, index, A)

                            if verbose:
                                print(
                                    "weight({},{},A_noloops)={} (a{})".format(
                                        m, index, w, "ok" if w < 0 else "breaking group"
                                    )
                                )

                            if w >= 0:
                                consistent_with_gc = False
                                break

                if not consistent_with_gc:
                    # we do not save the new group because of it being inconsistent given the last removed group
                    break

                # save group
                groups[iters, members_indices] = x[indices].flatten()
                if verbose:
                    print("groups:", groups)

            else:
                break

            iters += 1

        if affinity.shape[0] > 0:
            for i, person in enumerate(current_indices):
                groups[iters + i, person] = 1

        return groups


def find_dominant_sets_batch(
    A, n, device, thresh=1e-5, max_iter=500, min_eps=1e-15, stop_gc=True
):
    with torch.no_grad():
        B, N = A.shape[0], A.shape[1]
        out = torch.zeros((B, N, N), dtype=torch.double, device=device)
        for b in range(B):
            out[b, :, :] = dominant_sets_clustering(
                A, n[b], device, thresh, max_iter, min_eps, stop_gc=stop_gc
            )
        return out


class DS(nn.Module):
    def __init__(self, thresh, max_iter, min_eps, device, stop_gc):
        super(DS, self).__init__()

        self.thresh = thresh
        self.max_iter = max_iter
        self.min_eps = min_eps
        self.device = device
        self.stop_gc = stop_gc
        print("early stopping:", self.stop_gc)

    def forward(self, A, n):
        # TODO: vectorize DS
        if len(A.shape) == 2:
            return dominant_sets_clustering(
                A,
                n,
                self.device,
                thresh=self.thresh,
                max_iter=self.max_iter,
                min_eps=self.min_eps,
                stop_gc=self.stop_gc,
            )
        return find_dominant_sets_batch(
            A, n, self.device, self.thresh, self.max_iter, self.min_eps, self.stop_gc
        )
