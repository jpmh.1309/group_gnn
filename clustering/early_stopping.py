import torch


def awdeg(S, i, A):
    """
    Average weighted degree of vertex i in set S (eq. (3) in the paper)
    :param S: indicator vector (N) with 1 for the nodes in the set S
    :param i: index of the node i
    :param A: affinity matrix (NxN)
    """
    assert bool(S[i]), "i ({}) should be in S ({})".format(i, S)
    sum_affs = 0.0
    for j in range(len(S)):
        if S[j]:
            sum_affs += A[i, j]
    return sum_affs / torch.sum(S)


def phi(S, i, j, A):
    """
    Relative affinity between node j not in S and i (eq. (4) in the paper)
    :param S: indicator vector (N) with 1 for the nodes in the set S
    :param i: index of the node in S that we are computing the affinity for
    :param j: index of the node (not in S) that we are computing the affinity for
    :param A: affinity matrix (NxN)
    """
    return A[i, j] - awdeg(S, i, A)


def weight(S, i, A):
    """
    Dominant Sets weight function (eq. (5) in the paper)
    The function measures the overall relative affinity between node i and the rest of the vertices in S
    :param S: indicator vector (N) with 1 for the nodes in the set S
    :param i: index of the node that we are computing the weight for
    :param A: affinity matrix (NxN)
    """
    set_size = torch.sum(S.type(torch.int))
    if set_size < 1:
        raise RuntimeError(
            "The weight(S,i,A) function is only defined for nonempty sets (set_size = {}, S = {})".format(
                set_size, S.flatten()
            )
        )

    if set_size == 1:
        return 1.0

    R = S.detach().clone()
    R[i] = False

    sum_weights = 0.0
    for j in range(A.shape[0]):
        if not R[j]:
            continue  # vertex not in R
        sum_weights += phi(R, j, i, A) * weight(R, j, A)

    return sum_weights
