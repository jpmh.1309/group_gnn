def log_scalar(name, trainer):
    """
    Logs scalar to tensorboard params/ plot
    :param name: name of param to log under params/ tag
    :return: wrapper function called by tb_logger
    """

    def wrapper(engine, logger, event_name):
        print(
            f"max_iter: {trainer.state.model.ds.max_iter} - epochs: {trainer.state.epoch}"
        )
        logger.writer.add_scalar(
            f"params/{name}", trainer.state.model.ds.max_iter, trainer.state.epoch
        )

    return wrapper
