def log_hyperparams(tag, args, name):
    """
    Logs hyperparameters and most recent evaluation metrics
    :param tag: data tag
    :param args: hyperparameters of model
    :param global_step_transform: function that returns current epoch
    :return: wrapper function called by tb_logger
    """

    def wrapper(engine, logger, event_name):
        hparam_dict = {
            "dataset": args.dataset,
            "dropout": args.dropout,
            "early_stopping:": args.early_stopping,
            "epochs": args.epochs,
            "expand_features": args.expand_features,
            "lr": args.lr,
            "max_iter": args.max_iter,
            "model": args.model,
            "no_cuda": args.no_cuda,
            "patience": args.patience,
            "thres": args.thresh,
            "weight_decay": args.weight_decay,
            "random_sampler:": args.random_sampler,
            "lr_step": args.lr_step,
            "lr_decay": args.lr_decay,
        }

        logger.writer.add_hparams(
            hparam_dict=hparam_dict, metric_dict=engine.state.metrics
        )

    return wrapper
