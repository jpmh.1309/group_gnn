import os
import json

import torch
import torch.nn as nn

from helper.get_arguments import get_arguments

from main import setup, get_data_loaders_gnn

# ignores model parameters
args = get_arguments()

if args.dataset not in [
    "cocktail_party",
    "cocktail_party_head",
    "cocktail_party_body",
    "cocktail_party_all",
    "matchnmingle",
    "matchnmingle_accel_label",
    "matchnmingle_camera_0",
    "matchnmingle_camera_1",
    "matchnmingle_camera_2",
    "matchnmingle_4_layers",
    "matchnmingle_4_layers_camera_0",
    "matchnmingle_4_layers_camera_1",
    "matchnmingle_4_layers_camera_2",
]:
    raise NotImplementedError("Dataset Not Available")

# setup pytorch seed, GPU settings, etc
out_dir, device, use_cuda, model_name = setup(args)

if out_dir:
    with open(os.path.join(out_dir, "params.txt"), "w") as fid:
        json.dump(args.__dict__, fid, indent=2)

max_people = None  # only needed for DANTE

print("Using ", args.model, " Model")

# loads datasets, splits into train/val/test
(
    train_dataset,
    val_dataset,
    test_dataset,
    train_loader,
    train_metrics_loader,
    val_loader,
    test_loader,
) = get_data_loaders_gnn(args, device)


class Baseline(nn.Module):
    def __init__(self, device):
        super(Baseline, self).__init__()

        # self.x = nn.Parameter(torch.ones(1))
        self.x = torch.ones(1, requires_grad=False, device=device)

        self.device = device

    def forward(self, data):
        edges = data.edge_attr
        edges = torch.exp(-edges[:, 0] / self.x)

        data_n = data.x.shape[0]
        A = torch.zeros((data_n, data_n), dtype=torch.float, device=self.device)
        A[data.edge_index[0], data.edge_index[1]] = edges.flatten()

        output = []
        for i in range(data.ptr.shape[0] - 1):
            curr_ptr, next_ptr = data.ptr[i], data.ptr[i + 1]
            A_i = A[curr_ptr:next_ptr, curr_ptr:next_ptr]
            n = A_i.shape[0]
            triu = torch.triu_indices(n, n, 1)
            A_upper = A_i[triu[0], triu[1]]
            A_lower = A_i.T[triu[0], triu[1]]
            A_sym = (A_upper + A_lower) / 2
            A_new = torch.zeros_like(A_i)
            A_new[triu[0], triu[1]] = A_sym
            A_new = A_new + A_new.T
            output.append(A_new)
        return output


# initializes model
print("Loading baseline model...")
model = Baseline("cpu")

# create centralized trainer object
print("Getting trainer...")
# trainer, train_eval, val_eval, test_eval = get_trainer(
#     args, train_loader, val_loader, test_loader, model, out_dir, device
# )


# kick everything off
print("Running model...")
# trainer.run(train_loader, max_epochs=args.epochs)

print("Finished Training!")
