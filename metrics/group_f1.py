"""
F1 metric used for group evaluation.

The implementation is based on:

Cristani, M., Bazzani, L., Paggetti, G., Fossati, A., Tosato, D., Del Bue, A., ... & Murino, V. (2011, September).
Social interaction discovery by statistical analysis of F-formations. In BMVC (Vol. 2, p. 4).

Setti, F., Hung, H., & Cristani, M. (2013, July). Group detection in still images by F-formation modeling: A comparative
study. In 2013 14th International Workshop on Image Analysis for Multimedia Interactive Services (WIAMIS). IEEE.

Setti, F., Russell, C., Bassetti, C., & Cristani, M. (2015). F-formation detection: Individuating free-standing
conversational groups in images. PloS one, 10(5), e0123783.
"""

from datetime import datetime

from joblib import Parallel, delayed
import multiprocessing

import torch

from ignite.metrics import Metric

# These decorators helps with distributed settings
from ignite.metrics.metric import sync_all_reduce, reinit__is_reduced

from transforms import (
    num_people_from_num_dyads,
    unflatten_to_A,
    y_to_A,
    true_affinity_to_dataset_groups,
    ds_groups_to_dataset_groups,
)
from clustering.ds import dominant_sets_clustering


def group_f1(groups_pred, groups, threshold=1.0):
    """
    Compute group F1 score
    :param groups_pred: list of predicted groups (each group should be a list with ids)
    :param groups: list of ground truth groups (each group should be a list with ids)
    :param threshold: threshold (% participants) for classifying groups as TP
    :return: F1 metric, (precision, recall), (TP, FP, FN)
    """
    n_pred, n = len(groups_pred), len(groups)

    # handle degenerate cases first
    if n_pred == 0 and n == 0:
        TP, FP, FN = 0, 0, 0
        precision, recall = 1.0, 1.0
    elif n_pred == 0 and n > 0:
        TP, FP, FN = 0, 0, n
        precision, recall = 1.0, 0.0
    elif n_pred > 0 and n == 0:
        TP, FP, FN = 0, n_pred, 0
        precision, recall = 0.0, 1.0
    else:
        TP = 0

        # for each ground truth group
        for _, true_group_ids in enumerate(groups):
            # for each predicted group
            for _, pred_group_ids in enumerate(groups_pred):
                intersect = list(set(true_group_ids).intersection(set(pred_group_ids)))
                pred_correct = len(intersect)
                pred_incorrect = len(pred_group_ids) - pred_correct
                true_size = len(true_group_ids)
                if (
                    pred_correct >= threshold * true_size
                    and pred_incorrect <= (1 - threshold) * true_size
                ):
                    TP = TP + 1
                    break

        FP = n_pred - TP
        FN = n - TP

        precision = 1.0 * TP / (TP + FP)
        recall = 1.0 * TP / (TP + FN)

    f1 = 2 * (precision * recall) / (precision + recall + 1e-20)

    return f1, (precision, recall), (TP, FP, FN)


class GroupF1Metric(Metric):
    """
    Ignite metric for evaluating group f1 scores
    """

    def __init__(
        self,
        output_transform=lambda x: x,
        model="gnn",
        group_threshold=1.0,
        out="f1",
        threshold=1e-5,
        max_iter=None,
        min_eps=None,
        device=None,
        stop_gc=True,
        cores=1,
    ):
        super(GroupF1Metric, self).__init__(
            output_transform=output_transform, device=device
        )

        self.precision = None
        self.recall = None

        self.model = model

        self.group_threshold = group_threshold
        self.out = out

        self.threshold = threshold
        self.max_iter = max_iter
        self.min_eps = min_eps

        self.device = device
        self.stop_gc = stop_gc
        self.cores = cores

    @reinit__is_reduced
    def reset(self):
        super(GroupF1Metric, self).reset()
        self.precision = []
        self.recall = []

    def update_individual(self, y, y_pred):
        # handles different possible transforms
        if self.model == "gnn":
            y_pred_new = dominant_sets_clustering(
                y_pred.double(),
                y_pred.shape[0],
                self.device,
                thresh=self.threshold,
                max_iter=self.max_iter,
                min_eps=self.min_eps,
                stop_gc=self.stop_gc,
            )
            groups_pred = ds_groups_to_dataset_groups(y_pred_new, self.threshold)
            groups = true_affinity_to_dataset_groups(y_to_A(y))
        else:
            mask = y >= 0
            pred = y_pred[mask]
            truth = y[mask]

            num_people = num_people_from_num_dyads(pred.shape[0])
            A = unflatten_to_A(truth, num_people, torch.double, self.device)
            groups = true_affinity_to_dataset_groups(A)

            A = unflatten_to_A(pred, num_people, torch.double, self.device)
            groups_pred = ds_groups_to_dataset_groups(
                dominant_sets_clustering(
                    A,
                    num_people,
                    self.device,
                    thresh=self.threshold,
                    max_iter=self.max_iter,
                    min_eps=self.min_eps,
                    stop_gc=self.stop_gc,
                ),
                thres=self.threshold,
            )

        _, (pr, re), _ = group_f1(groups_pred, groups, threshold=self.group_threshold)
        return pr, re

    @reinit__is_reduced
    def update(self, output):
        start = datetime.now()
        y_pred, y = output
        if self.cores == 1:
            for i in range(len(y)):
                pr, re = self.update_individual(y[i], y_pred[i])
                self.precision.append(pr)
                self.recall.append(re)
        else:
            num_cores = min(self.cores, multiprocessing.cpu_count())
            results = Parallel(n_jobs=num_cores, verbose=0, prefer="threads")(
                delayed(self.update_individual)(y_i, y_pred_i)
                for y_i, y_pred_i in zip(y, y_pred)
            )
            for pr, re in results:
                self.precision.append(pr)
                self.recall.append(re)
        end = datetime.now()
        print("Time Elapsed:", end - start)

    @sync_all_reduce("precision", "recall")
    def compute(self):
        avg_precision = sum(self.precision) / (len(self.precision) + 1e-15)
        avg_recall = sum(self.recall) / (len(self.recall) + 1e-15)
        f1 = 2.0 * avg_precision * avg_recall / (avg_precision + avg_recall + 1e-15)

        if self.out == "f1":
            return f1
        elif self.out == "pr":
            return avg_precision
        else:
            return avg_recall
