import numpy as np
import torch

# TODO: clean up, simplify transforms


def groups_to_affinity(groups, max_people, device):
    A = torch.zeros((max_people, max_people), dtype=torch.bool, device=device)
    for group in groups:
        for i, p1 in enumerate(group):
            for p2 in group[i + 1 :]:
                A[p1][p2] = True
                A[p2][p1] = True
    return A


def y_to_A(y):
    n = y.shape[0]
    y1 = torch.tile(y, (n, 1)).flatten()
    y2 = torch.repeat_interleave(y, n, dim=0)
    A = (y1 == y2).reshape(n, n)
    # TODO: make inplace
    for i in range(n):
        A[i, i] = 0
    return A.float()


def A_to_A_list(A):
    n = A.shape[0]
    triu = torch.triu_indices(n, n, 1)
    A_list = A[triu[0], triu[1]]
    return A_list


def y_to_A_list(y):
    return A_to_A_list(y_to_A(y))


def true_affinity_to_dataset_groups(A):
    """
    Convert true affinity matrix with 0s and 1s to a list of groups
    :param A: affinity matrix
    :return: list of groups indexed by people's row/col in A
    """
    N = A.shape[0]
    groups = []
    got_group = [False for _ in range(N)]
    for i in range(N):
        if got_group[i]:
            continue
        i_group = [i]
        got_group[i] = True
        for j in range(i + 1, N):
            if A[i, j] > 0 and not got_group[j]:
                i_group.append(j)
                got_group[j] = True
        groups.append(i_group)
    return groups


def ds_groups_to_dataset_groups(ds_groups, thres):
    """
    Converts from list of memberships to list of groups
    [[0, 1, 1], [1, 0, 0]] => [[1, 2], [0]]
    :param ds_groups: input groups formatted as DANTE w/ DS outputs
    :return dataset_groups: output groups formatted as list of groups
    """

    groups = []
    for group in ds_groups:
        group_indices = []
        for i, val in enumerate(list(group)):
            if val > thres:
                group_indices.append(i)
        if len(group_indices) > 0:
            groups.append(group_indices)
    return groups


def _unflatten_to_A_batch(A_list, n, dtype, device):
    A_list = A_list.view(-1, round(n * (n - 1) / 2))
    A = torch.zeros(size=(A_list.shape[0], n, n), dtype=dtype, device=device)
    triu_indices = torch.triu_indices(n, n, 1)
    A[:, triu_indices[0], triu_indices[1]] = A_list.type(dtype)
    A = A + torch.transpose(A, -2, -1)
    return A


def unflatten_to_A_batch(A_list, n, dtype, device):
    num_pairs = int(n * (n - 1) / 2)
    A_list = A_list.view(-1, n * (n - 1))
    A_list_top = A_list[:, :num_pairs]
    A_list_bottom = A_list[:, num_pairs:].flip(-1)

    A_top = _unflatten_to_A_batch(A_list_top, n, dtype, device)
    A_bottom = _unflatten_to_A_batch(A_list_bottom, n, dtype, device)
    return (A_top + A_bottom) / 2


def unflatten_to_A(A_list, n, dtype, device):
    return unflatten_to_A_batch(A_list, n, dtype, device)[0]


def num_people_from_num_dyads(d):
    # If we have N*N - N = D, then N^2 - N - D = 0. Solving for the roots and keeping the positive result gives...
    return int((1.0 + (1.0 + 4.0 * d) ** 0.5) * 0.5)
