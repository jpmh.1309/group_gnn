import torch
import torch.nn as nn

from models.dante.context_net import ContextNet
from models.dante.dyad_net import DyadNet
from models.dante.expansion_model import ExpansionModel
from transforms import unflatten_to_A_batch


class DANTE(nn.Module):
    """DANTE model"""

    def __init__(
        self,
        input_feat,
        dyad_mlp_dims=None,
        context_mlp_dims=None,
        max_pool=True,
        dense_dims=[1024, 256],
        add_batchnorm=True,
        add_layernorm=False,
        dropout=None,
        max_people=None,
        device=None,
        dtype=None,
        expand_features=0,
        expand_dims=[],
        img_mlp_features=0,
        img_mlp_dims=[],
    ):
        """
        Constructor
        :param input_feat: number of features per person in the input
        :param dyad_mlp_dims: number of features in the individual mlp of the dyad transform
        :param context_mlp_dims: number of features in the individual mlp of the context transform
        :param max_pool: if True, use max pooling to combine context features; otherwise, use avg. pooling
        :param dense_dims: list with dimensions of the last dense layers
        :param add_batchnorm: add batchnorm? (default: True)
        :param dropout: dropout probability (default: None, no dropout)
        :param max_people: max_people to pad output tensors (default: None, must be supplied)
        :param device: determines whether to use GPU or CPU (default: None, must be supplied)
        :param dtype: determines data type to store tensors (default: None, must be supplied)
        """
        super(DANTE, self).__init__()

        self.device = device
        self.dtype = dtype

        self.max_people = max_people

        self.expand_features = expand_features
        if self.expand_features > (input_feat - img_mlp_features):
            raise ValueError(
                f"Cannot have more expand_features than input features (less img mlp features) - expand_features: {self.expand_features} - input_feat: {input_feat}"
            )
        if expand_features > 0 and len(expand_dims) > 0:
            self.expansion_model = ExpansionModel(expand_features, expand_dims)
            input_feat = input_feat - self.expand_features + expand_dims[-1]
        else:
            self.expansion_model = None

        self.img_mlp_features = img_mlp_features
        if self.img_mlp_features > (input_feat - self.expand_features):
            raise ValueError(
                f"Cannot have more img_mlp than input features (less expansion features) - expand_features: {self.img_mlp_features} - input_feat: {input_feat} - expand_features: {self.expand_features}"
            )
        if self.img_mlp_features > 0 and len(img_mlp_dims) > 0:
            self.img_mlp = ExpansionModel(self.img_mlp_features, img_mlp_dims)
            input_feat = input_feat - self.img_mlp_features + img_mlp_dims[-1]
        elif self.img_mlp_features == -1:
            input_feat = input_feat - 512
            self.img_mlp = None
        else:
            self.img_mlp = None

        """Create upstream layers"""

        # note that by default, we've turned off dropout in upstream layers
        self.dyad_transform = DyadNet(
            input_feat,
            mlp_dims=dyad_mlp_dims,
            add_batchnorm=add_batchnorm,
            add_layernorm=add_layernorm,
            dropout=0,
        )

        self.context_transform = ContextNet(
            input_feat,
            mlp_dims=context_mlp_dims,
            max_pool=max_pool,
            add_batchnorm=add_batchnorm,
            add_layernorm=add_layernorm,
            dropout=0,
        )

        self.context_dim = context_mlp_dims[-1]
        self.dense_dims = dense_dims

        """Create downstream layers"""

        # calculate dense mlp input dim
        input_dim = self.dyad_transform.mlp_dims[-1] * 2
        input_dim += self.context_transform.mlp_dims[-1]

        self.fc_layers = nn.ModuleList([])

        # apply dropout
        if dropout > 0:
            self.fc_layers.append(nn.Dropout(dropout))

        for i in range(len(self.dense_dims)):
            self.fc_layers.append(nn.Linear(input_dim, self.dense_dims[i]))
            input_dim = self.dense_dims[i]
            self.fc_layers.append(nn.ReLU())
            # include dropout
            if dropout > 0:
                self.fc_layers.append(nn.Dropout(dropout))
            # we apply BN after the activation
            # https://blog.paperspace.com/busting-the-myths-about-batch-normalization/
            if add_batchnorm:
                self.fc_layers.append(nn.BatchNorm1d(input_dim))
            if add_layernorm:
                self.fc_layers.append(nn.LayerNorm(input_dim))

        self.last_layer = nn.Linear(input_dim, 1)

    def _forward(self, x_list):
        """
        Forward operation
        :param x_list: list of tuples with (dyad_input tensor, context_input tensor, num_people_input tensor), bucketed by num_people
        :note dyad_input is expected to be (N, 2, F) where N is number of samples, 2 is #people, and F is #feat per person
        :note context_input is expected to be (N, P, F) where N is number of samples, P is #people, and F is #feat per person
        :note num_people_input is expected to be a single torch item (0 dimensional)
        :return: output tensor (N, 1) with the classification prob (after softmax)
        """
        if not isinstance(x_list, list):
            x_list = [x_list]

        B = sum([x[0].shape[0] for x in x_list])  # batch size
        N = self.max_people
        out_full = torch.zeros((B, N, N), dtype=self.dtype, device=self.device)
        people_inputs = torch.zeros((B), dtype=self.dtype, device=self.device)

        index = 0
        for x_same_size in x_list:
            no_context = False
            dyad_input, context_input, num_people_input = x_same_size
            B_same_size = num_people_input.shape[0]
            num_people = int(num_people_input[0].item())  # constant within x_same_size
            num_pairs = num_people * (num_people - 1)  # constant within x_same_size
            context_pairs = context_input.shape[2]

            d = dyad_input.view(-1, dyad_input.shape[-2], dyad_input.shape[-1])
            if context_pairs == 0:  # two people left in frame, no context
                no_context = True
            else:
                c = context_input.view(
                    -1, context_input.shape[-2], context_input.shape[-1]
                )

            if self.expansion_model:
                d_expanded_features = self.expansion_model(
                    d[:, :, : self.expand_features]
                )
                d = torch.cat((d_expanded_features, d[:, :, self.expand_features :]), 2)

                if not no_context:
                    c_expanded_features = self.expansion_model(
                        c[:, :, : self.expand_features]
                    )
                    c = torch.cat(
                        (c_expanded_features, c[:, :, self.expand_features :]), 2
                    )

            if self.img_mlp:
                d_img_features = self.img_mlp(d[:, :, -self.img_mlp_features :])
                d = torch.cat((d[:, :, : -self.img_mlp_features], d_img_features), 2)

                if not no_context:
                    c_expanded_features = self.img_mlp(
                        c[:, :, -self.img_mlp_features :]
                    )
                    c = torch.cat(
                        (c[:, :, : -self.img_mlp_features], c_expanded_features), 2
                    )
            elif self.img_mlp_features == -1:
                d = d[:, :, :-512]
                if not no_context:
                    c = c[:, :, :-512]

            # compute dyad and context features
            dyad_feature = self.dyad_transform(d)
            if no_context:
                context_feature = torch.zeros(
                    (B_same_size * num_pairs, self.context_dim),
                    dtype=self.dtype,
                    device=self.device,
                )
            else:
                context_feature = self.context_transform(c)

            # concatenate features
            features = torch.cat((dyad_feature, context_feature), dim=1)
            features = features.view(-1, features.shape[-1])

            # dense layers
            for fc_layer in self.fc_layers:
                if isinstance(fc_layer, nn.BatchNorm1d):
                    if features.shape[0] > 1:
                        features = fc_layer(features)
                else:
                    features = fc_layer(features)

            # finally pass through a sigmoid to produce a prob value in [0, 1]
            out = torch.sigmoid(self.last_layer(features)).view(num_people, -1)
            out = unflatten_to_A_batch(out, num_people, torch.double, self.device)
            out_full[index : index + out.shape[0], :num_people, :num_people] = out
            people_inputs[index : index + out.shape[0]] = num_people
            index += out.shape[0]

        mask_top = torch.triu_indices(N, N, 1)
        out_full_top = out_full[:, mask_top[0], mask_top[1]]
        mask_bottom = torch.tril_indices(N, N, -1)
        out_full_bottom = out_full[:, mask_bottom[0], mask_bottom[1]].flip(-1)
        out_full = torch.cat((out_full_top, out_full_bottom), dim=1)
        return out_full

    def forward(self, x_list):
        return self._forward(x_list)
