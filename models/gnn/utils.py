import torch.nn as nn

from torch_geometric.nn import MetaLayer

from models.gnn.edge_model import EdgeModel
from models.gnn.node_model import NodeModel


def build_layer(
    n_features,
    n_edge_features,
    n_edge_hiddens,
    n_edge_targets,
    n_node_hiddens,
    n_node_targets,
    dropout,
):
    return MetaLayer(
        edge_model=EdgeModel(
            n_features=n_features,
            n_edge_features=n_edge_features,
            n_hiddens=n_edge_hiddens,
            n_targets=n_edge_targets,
            dropout=dropout,
        ),
        node_model=NodeModel(
            n_features=n_features,
            n_edge_features=n_edge_targets,
            n_hiddens=n_node_hiddens,
            n_targets=n_node_targets,
            dropout=dropout,
        ),
    )


class ListModule(object):
    # Should work with all kind of module
    def __init__(self, module, prefix, *args):
        self.module = module
        self.prefix = prefix
        self.num_module = 0
        for new_module in args:
            self.append(new_module)

    def append(self, new_module):
        if not isinstance(new_module, nn.Module):
            raise ValueError("Not a Module")
        else:
            self.module.add_module(self.prefix + str(self.num_module), new_module)
            self.num_module += 1

    def __len__(self):
        return self.num_module

    def __getitem__(self, i):
        if i < -self.num_module or i >= self.num_module:
            raise IndexError("Out of bound")
        i = i % self.num_module
        return getattr(self.module, self.prefix + str(i))
